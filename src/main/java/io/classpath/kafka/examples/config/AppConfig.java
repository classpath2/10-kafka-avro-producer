package io.classpath.kafka.examples.config;

public class AppConfig {
    public final static String applicationID = "PosSimulator";
    public final static String bootstrapServers = "68.183.95.195:9092,68.183.80.182:9092,68.183.88.114:9092";
    public final static String schemaRegistryServers = "http://159.65.151.85:8081";
}
